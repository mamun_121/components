import React from 'react';


class CommentDetails extends React.Component{
    render() {
        const{author, timeAgo, content, avatar} = this.props;
        return(
            <div className="comment">
                <a className="avatar" href='/'>
                    <img src={avatar}alt='Avatar'/>
                </a>
                <div className="content">
                    <a className="author" href='/'>{author}</a>
                    <div className="metadata">
                        <span className="date">{timeAgo}</span>
                    </div>
                    <div className="text">
                        {content}
                    </div>
                </div>
            </div>
        );

    }
}

export default CommentDetails;