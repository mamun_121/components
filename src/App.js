import React from 'react';
import './App.css';
import faker from "faker";
import CommentDetails from "./components/CommentDetails";
import ApprovalCard from "./components/ApprovalCard";

function App() {

  return (

      <div className="ui container comments">
        <h3 className="ui dividing header">Comments</h3>
          <ApprovalCard>
              <CommentDetails
                  author="Sam"
                  timeAgo="Today at 4:45PM"
                  content="Nice blog post"
                  avatar={faker.image.avatar()}
              />
          </ApprovalCard>


          <ApprovalCard>
              <CommentDetails
                  author="Alex"
                  timeAgo="Today at 2:00AM"
                  content="I like the subject"
                  avatar={faker.image.avatar()}
              />
          </ApprovalCard>

          <ApprovalCard>
              <CommentDetails
                  author="Jane"
                  timeAgo="Yesterday at 5:00PM"
                  content="I like the writing"
                  avatar={faker.image.avatar()}
              />

          </ApprovalCard>

      </div>
  );
}

export default App;
